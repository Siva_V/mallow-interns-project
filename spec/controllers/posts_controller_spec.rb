require 'rails_helper'

RSpec.describe "/topics", type: :request do

  let(:valid_attributes){ {name:'Comic'}
  }

  let(:invalid_attributes){{name:'name'}
  }

  describe 'GET /index' do
    it 'Checks for presence of a datas' do
      get topic_posts_url
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'Checking' do
      get new_topic_post_path
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'Checking the path edit' do
      edit = Topic.create! valid_attributes
      get edit_topic_post_path(edit)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Topic" do
        expect {
          post topic_posts_path, params: { topic: valid_attributes }
        }.to change(Post, :count).by(1)
      end

      it "redirects to the created topic" do
        post topics_url, params: { topic: valid_attributes }
        expect(response).to redirect_to(topic_url(Topic.last))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Topic" do
        expect {
          post topics_url, params: { topic: invalid_attributes }
        }.to change(Topic, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        post topics_url, params: { topic: invalid_attributes }
        #expect(response).to be_successful
        is_expected.to render_template(:new)
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes){ {name:'we'}
      }

      it "updates the requested topic" do
        topic = Topic.create! valid_attributes
        patch topic_url(topic), params: { topic: new_attributes }
        topic.reload
      end

      it "redirects to the topic" do
        topic = Topic.create! valid_attributes
        patch topic_url(topic), params: { topic: new_attributes }
        topic.reload
        expect(response).to redirect_to(topics_path)
      end
    end

    context "with invalid parameters" do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        topic = Topic.create! valid_attributes
        patch topic_url(topic), params: { topic: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested topic" do
      topic = Topic.create! valid_attributes
      expect {
        delete topic_post_url(topic)
      }.to change(Post, :count).by(-1)
    end

    it "redirects to the topics list" do
      topic = Topic.create! valid_attributes
      delete topic_url(topic)
      expect(response).to redirect_to(topics_url)
    end
  end
end