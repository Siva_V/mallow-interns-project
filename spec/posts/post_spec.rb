require 'rails_helper'

RSpec.describe "/posts", type: :request do

  let(:create_topic) {
    @topic = Topic.create! vvalid_attributes
  }
  let(:vvalid_attributes){
    { name: 'name' }
  }
  let(:valid_attributes) {
    create_topic
    { name: 'name', content: 'content', topic_id: @topic.id}
    #skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    { name: '', content: 'y', topic_id: @topic.id }
    #skip("Add a hash of attributes invalid for your model")
  }
  let(:create_post) {
    @post = Post.create! valid_attributes
  }

  describe "GET /index" do
    it "renders a successful response" do
      create_post
      get topic_posts_url(topic_id: @topic.id)
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      create_post#post = Post.create! valid_attributes
      get topic_posts_url(topic_id: @topic.id)
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it "renders a successful response" do
      create_post
      get new_topic_post_url(topic_id: @topic.id)
      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "renders a successful response" do
      create_post
      get edit_topic_post_url(topic_id:@topic.id, id:@post.id)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Post" do
        create_post
        expect {
          post topic_posts_url(topic_id: @topic.id, id:@post.id), params: { post: valid_attributes }
        }.to change(Post, :count).by(1)
      end

      it "redirects to the created post" do
        create_post
        post topic_posts_url(topic_id: @topic.id, id:@post.id), params: { post: valid_attributes }
        expect(response).to redirect_to(topic_posts_url)
      end
    end

    context "with invalid parameters" do
      it "does not create a new Post" do
        create_post
        expect {
          post topic_posts_url(topic_id:@topic.id), params: { post: invalid_attributes }
        }.to change(Post, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        create_post
        post topic_posts_url(topic_id:@topic.id, id:@post.id), params: { post: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) {
        { name:'name', content:"content", topic_id:@topic.id}
      }

      it "updates the requested post" do
        #post = Post.create! valid_attributes
        p=create_post
        patch topic_post_url(topic_id=@topic.id, id:@post.id), params: { post: new_attributes }
        p.reload
        #skip("Add assertions for updated state")
      end

      it "redirects to the post" do
        p=create_post#post = Post.create! valid_attributes
        patch topic_post_url(topic_id:@topic.id, id:@post.id), params: { post: new_attributes }
        p.reload
        expect(response).to redirect_to(topic_post_url)
      end
    end

    context "with invalid parameters" do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        #post = Post.create! valid_attributes
        create_post
        patch topic_post_url(topic_id:@topic.id, id:@post.id), params: { post: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested post" do
      create_post
      expect {
        delete topic_post_url(topic_id: @topic.id, id:@post.id)
      }.to change(Post, :count).by(-1)
    end

    it "redirects to the posts list" do
      #post = Post.create! valid_attributes
      create_post
      delete topic_post_url(topic_id:@topic.id, id:@post.id)
      expect(response).to redirect_to(topic_posts_url(topic_id:@topic.id))
    end
  end
end