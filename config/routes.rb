Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :topics do
    resources :posts
  end
  root 'topics#index'
  get 'posts' => 'posts#index', as: :posts
end