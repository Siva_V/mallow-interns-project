class Topic < ApplicationRecord
  has_many :posts
  validates :name, length: { minimum:1 }
  accepts_nested_attributes_for :posts
end