class TopicsController < ApplicationController
  def index
    @topic = Topic.all
  end

  def show
    @show = Topic.find(params[:id])
  end

  def new
    @new = Topic.new
  end

  def create
    @create = Topic.new(get)
    if @create.save
      redirect_to topics_path
    else
      render :new
    end
  end

  def edit
    @edit = Topic.find(params[:id])
  end

  def update
    @update = Topic.find(params[:id])
    if @update.update(get)
      redirect_to topics_path
    else
      render :edit
    end
  end

  def destroy
    @destroy = Topic.find(params[:id])
    @destroy.destroy
    redirect_to topics_path
  end

  private
    def get
      params.require(:topic).permit(:name)
    end
end
