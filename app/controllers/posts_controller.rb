class PostsController < ApplicationController

  before_action :getting

  def index
    @post = @data.posts
    #@post1 = Post.where(:topic_id => @post)
  end

  def show
    @show = Topic.find(params[:id])
  end

  def new
    @new = Post.new
  end

  def create
    @create = @data.posts.build(gets)
    if @create.save
      redirect_to topic_posts_path
    end
  end

  def edit
    @edit = @data.posts.find(params[:id])
  end

  def update
    @update = Post.find(params[:id])
    if @update.update(get)
      redirect_to topic_post_path(@data)
    end
  end

  def destroy
    @destroy = Post.find(params[:id])
    @destroy.destroy
    redirect_to topic_posts_path(@data)
  end
  private
    def get
      params.require(:post).permit(:name, :content)
    end

    def getting
      @data = Topic.find(params[:topic_id])
    end

    def gets
      params.require(:post).permit(:name, :content, :topic_id)
    end
end
